app.run(function ($rootScope, $state, $http, $log) {

});

app.controller('mainController',function($rootScope,$scope,$http){
  
});
app.controller('homeController',function($rootScope,$scope,$http){

});
app.controller('browseListingController',function($rootScope,$scope,$http){
  $('select').material_select();
  $('input[type=checkbox]').each(function() {
    if(this.nextSibling.nodeName != 'label') {
      $(this).after('<label for="'+this.id+'"></label>')
    }
  });
  var slider = document.getElementById('investment-slider');
  // noUiSlider.create(slider, {
  //  start: [20, 80],
  //  connect: true,
  //  step: 1,
  //  orientation: 'horizontal', // 'horizontal' or 'vertical'
  //  range: {
  //    'min': 0,
  //    'max': 100
  //  },
  //  format: wNumb({
  //    decimals: 0
  //  })
  // });
});

app.controller('browseListingDetailsController',function($rootScope,$scope,$http){
  
});

app.controller('signupController',function($rootScope,$scope,$http){
  
});

app.controller('loginController',function($rootScope,$scope,$http){

});

app.controller('emailController',function($rootScope,$scope,$http){
  
});

app.controller('dashboardController',function($rootScope,$scope,$http){

});

app.controller('requestAccessController',function($rootScope,$scope,$http){
  
});

app.controller('browseListingDetailsAuthorisedController',function($rootScope,$scope,$http){
  $('.tabs').tabs();
  $(document).ready(function() {
    $('.modal').modal();
    $('select').material_select();
    //M.updateTextFields();
  });
});

app.controller('serviceProvidersListController',function($rootScope,$scope,$http){
  $('select').material_select();
  $('input[type=checkbox]').each(function() {
    if(this.nextSibling.nodeName != 'label') {
      $(this).after('<label for="'+this.id+'"></label>')
    }
  });
  var slider = document.getElementById('investment-slider');
  //bookmark
  $scope.setBookmark = function(data){
    var bookmarkStatus = '';
    if(data == 'yes'){
      bookmarkStatus = 'bookmark';
    }else{
      bookmarkStatus = 'bookmark_border';
    }
    return bookmarkStatus;
  }
  $scope.bookmark = function(data){
     var data = !data;
     $scope.setBookmark(data);
  }
});

app.controller('serviceProvidersDetailsController',function($rootScope,$scope,$http){
  $('.tabs').tabs();
  $(document).ready(function() {
    setTimeout(() => {
      $('select').material_select();
      $('.modal').modal();
    }, 1000);
  });
});

app.controller('postListingController',function($rootScope,$scope,$http){
  $(document).ready(function(){
    $('select').material_select();
  });
});

app.controller('postListingBasicInformationController',function($rootScope,$scope,$http){
    $('select').material_select();
});

app.controller('postListingContactInformationController',function($rootScope,$scope,$http){
  
});

app.controller('postListingRegistrationController',function($rootScope,$scope,$http){
  $('select').material_select();
});

app.controller('postListingListingController',function($rootScope,$scope,$http){
  $('.datepicker').datepicker();
});

app.controller('postListingFinancialsController',function($rootScope,$scope,$http){
  
});

app.controller('settingsController',function($rootScope,$scope,$http){
  $(document).ready(function(){
    $("a").click(function(e) {
      e.preventDefault();
      var section = $(this).attr("href");
      $("html, body").animate({
        scrollTop: $(section).offset().top
      });
    });
    $('.scrollspy').scrollSpy();
  });
});