var app = angular.module('linkApp', ['ui.router']);
app.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/home'),
      $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: '/partials/home.html',
            controller: 'homeController'
        })
        .state('browseListing', {
            url: '/browse-listing',
            templateUrl: '/partials/browse-listing.html',
            controller: 'browseListingController'
        })
        .state('browseListingDetails', {
            url: '/browse-listing/details',
            templateUrl: '/partials/browse-listing-details-unauthorised.html',
            controller: 'browseListingDetailsController'
        }).state('signup', {
            url: '/signup',
            templateUrl: '/partials/signup.html',
            controller: 'signupController'
        }).state('login', {
            url: '/login',
            templateUrl: '/partials/login.html',
            controller: 'loginController'
        }).state('emailVerify', {
            url: '/verify-email',
            templateUrl: '/partials/emailVerify.html',
            controller: 'emailController'
        }).state('dashboard', {
            url: '/dashboard',
            templateUrl: '/partials/dashboard.html',
            controller: 'dashboardController'
        }).state('requestAccess', {
            url: '/request-access',
            templateUrl: '/partials/requestAccess.html',
            controller: 'requestAccessController'
        }).state('browseListingDetailsAuthorised', {
            url: '/browse-listing/details/id',
            templateUrl: '/partials/browse-listing-details-authorised.html',
            controller: 'browseListingDetailsAuthorisedController'
        }).state('serviceProvidersList', {
            url: '/service-provider-list',
            templateUrl: '/partials/serviceProvidersList.html',
            controller: 'serviceProvidersListController'
        }).state('serviceProvidersDetails', {
            url: '/service-provider-details',
            templateUrl: '/partials/serviceProviderDetails.html',
            controller: 'serviceProvidersDetailsController'
        }).state('postListing', {
            url: '/post-listing',
            templateUrl: '/partials/postListing.html',
            controller: 'postListingController'
        }).state('postListing.basicInformation', {
            url: '/basic-information',
            views: {
                "postListingView@postListing": {
                    templateUrl: '/partials/postListingBasicInformation.html',
                    controller: 'postListingBasicInformationController'
                }
            }
        }).state('postListing.contactInformation', {
            url: '/contact-information',
            views: {
                "postListingView@postListing": {
                    templateUrl: '/partials/postListingContactInformation.html',
                    controller: 'postListingContactInformationController'
                }
            }
        }).state('postListing.registration', {
            url: '/registration',
            views: {
                "postListingView@postListing": {
                    templateUrl: '/partials/postListingRegistration.html',
                    controller: 'postListingRegistrationController'
                }
            }
        }).state('postListing.listing', {
            url: '/listing-and-complaince',
            views: {
                "postListingView@postListing": {
                    templateUrl: '/partials/postListingListing.html',
                    controller: 'postListingListingController'
                }
            }
        }).state('postListing.financials', {
            url: '/financials',
            views: {
                "postListingView@postListing": {
                    templateUrl: '/partials/postListingFinancials.html',
                    controller: 'postListingFinancialsController'
                }
            }
        }).state('settings', {
            url: '/settings',
            templateUrl: '/partials/settings.html',
            controller: 'settingsController'
        });
});
